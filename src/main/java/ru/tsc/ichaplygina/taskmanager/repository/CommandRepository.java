package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.model.Command;

import java.util.ArrayList;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.ArgumentConst.*;
import static ru.tsc.ichaplygina.taskmanager.constant.CommandConst.*;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(CMD_ABOUT,
            "show developer info",
            ARG_ABOUT);

    private static final Command HELP = new Command(CMD_HELP,
            "show this message",
            ARG_HELP);

    private static final Command VERSION = new Command(CMD_VERSION,
            "show version info",
            ARG_VERSION);

    private static final Command EXIT = new Command(CMD_EXIT,
            "quit");

    private static final Command INFO = new Command(CMD_INFO,
            "show system info",
            ARG_INFO);

    private static final Command LIST_COMMANDS = new Command(CMD_LIST_COMMANDS,
            "list available commands");

    private static final Command LIST_ARGUMENTS = new Command(CMD_LIST_ARGUMENTS,
            "list available command-line arguments");

    private static final Command LIST_TASKS = new Command(TASKS_LIST,
            "show all tasks");

    private static final Command LIST_TASKS_BY_PROJECT = new Command(TASKS_LIST_BY_PROJECT,
            "show all tasks in a project");

    private static final Command ADD_TASK_TO_PROJECT = new Command(TASK_ADD_TO_PROJECT,
            "add task to a project");

    private static final Command CREATE_TASK = new Command(TASK_CREATE,
            "create a new task");

    private static final Command CLEAR_TASKS = new Command(TASKS_CLEAR,
            "delete all tasks");

    private static final Command SHOW_TASK_BY_ID = new Command(TASK_SHOW_BY_ID,
            "show task by id");

    private static final Command SHOW_TASK_BY_INDEX = new Command(TASK_SHOW_BY_INDEX,
            "show task by index");

    private static final Command SHOW_TASK_BY_NAME = new Command(TASK_SHOW_BY_NAME,
            "show task by name");

    private static final Command UPDATE_TASK_BY_INDEX = new Command(TASK_UPDATE_BY_INDEX,
            "update task by index");

    private static final Command UPDATE_TASK_BY_ID = new Command(TASK_UPDATE_BY_ID,
            "update task by id");

    private static final Command REMOVE_TASK_FROM_PROJECT = new Command(TASK_REMOVE_FROM_PROJECT,
            "remove task from a project");

    private static final Command REMOVE_TASK_BY_INDEX = new Command(TASK_REMOVE_BY_INDEX,
            "remove task by index");

    private static final Command REMOVE_TASK_BY_ID = new Command(TASK_REMOVE_BY_ID,
            "remove task by id");

    private static final Command REMOVE_TASK_BY_NAME = new Command(TASK_REMOVE_BY_NAME,
            "remove task by name");

    private static final Command START_TASK_BY_INDEX = new Command(TASK_START_BY_INDEX,
            "start task by index");

    private static final Command START_TASK_BY_ID = new Command(TASK_START_BY_ID,
            "start task by id");

    private static final Command START_TASK_BY_NAME = new Command(TASK_START_BY_NAME,
            "start task by name");

    private static final Command COMPLETE_TASK_BY_INDEX = new Command(TASK_COMPLETE_BY_INDEX,
            "complete task by index");

    private static final Command COMPLETE_TASK_BY_ID = new Command(TASK_COMPLETE_BY_ID,
            "complete task by id");

    private static final Command COMPLETE_TASK_BY_NAME = new Command(TASK_COMPLETE_BY_NAME,
            "complete task by name");

    private static final Command LIST_PROJECTS = new Command(PROJECTS_LIST,
            "show all projects");

    private static final Command CREATE_PROJECT = new Command(PROJECT_CREATE,
            "create a new project");

    private static final Command CLEAR_POJECTS = new Command(PROJECTS_CLEAR,
            "delete all projects");

    private static final Command SHOW_PROJECT_BY_ID = new Command(PROJECT_SHOW_BY_ID,
            "show project by id");

    private static final Command SHOW_PROJECT_BY_INDEX = new Command(PROJECT_SHOW_BY_INDEX,
            "show project by index");

    private static final Command SHOW_PROJECT_BY_NAME = new Command(PROJECT_SHOW_BY_NAME,
            "show project by name");

    private static final Command UPDATE_PROJECT_BY_INDEX = new Command(PROJECT_UPDATE_BY_INDEX,
            "update project by index");

    private static final Command UPDATE_PROJECT_BY_ID = new Command(PROJECT_UPDATE_BY_ID,
            "update project by id");

    private static final Command REMOVE_PROJECT_BY_INDEX = new Command(PROJECT_REMOVE_BY_INDEX,
            "remove project by index");

    private static final Command REMOVE_PROJECT_BY_ID = new Command(PROJECT_REMOVE_BY_ID,
            "remove project by id");

    private static final Command REMOVE_PROJECT_BY_NAME = new Command(PROJECT_REMOVE_BY_NAME,
            "remove project by name");

    private static final Command START_PROJECT_BY_INDEX = new Command(PROJECT_START_BY_INDEX,
            "start project by index");

    private static final Command START_PROJECT_BY_ID = new Command(PROJECT_START_BY_ID,
            "start project by id");

    private static final Command START_PROJECT_BY_NAME = new Command(PROJECT_START_BY_NAME,
            "start project by name");

    private static final Command COMPLETE_PROJECT_BY_INDEX = new Command(PROJECT_COMPLETE_BY_INDEX,
            "complete project by index");

    private static final Command COMPLETE_PROJECT_BY_ID = new Command(PROJECT_COMPLETE_BY_ID,
            "complete project by id");

    private static final Command COMPLETE_PROJECT_BY_NAME = new Command(PROJECT_COMPLETE_BY_NAME,
            "complete project by name");

    private static final Command[] COMMANDS = {
            LIST_TASKS, CREATE_TASK, CLEAR_TASKS,
            SHOW_TASK_BY_ID, SHOW_TASK_BY_INDEX, SHOW_TASK_BY_NAME,
            UPDATE_TASK_BY_INDEX, UPDATE_TASK_BY_ID,
            REMOVE_TASK_BY_INDEX, REMOVE_TASK_BY_ID, REMOVE_TASK_BY_NAME,
            START_TASK_BY_INDEX, START_TASK_BY_ID, START_TASK_BY_NAME,
            COMPLETE_TASK_BY_INDEX, COMPLETE_TASK_BY_ID, COMPLETE_TASK_BY_NAME,
            LIST_TASKS_BY_PROJECT, ADD_TASK_TO_PROJECT, REMOVE_TASK_FROM_PROJECT,
            LIST_PROJECTS, CREATE_PROJECT, CLEAR_POJECTS,
            SHOW_PROJECT_BY_ID, SHOW_PROJECT_BY_INDEX, SHOW_PROJECT_BY_NAME,
            UPDATE_PROJECT_BY_INDEX, UPDATE_PROJECT_BY_ID,
            REMOVE_PROJECT_BY_INDEX, REMOVE_PROJECT_BY_ID, REMOVE_PROJECT_BY_NAME,
            START_PROJECT_BY_INDEX, START_PROJECT_BY_ID, START_PROJECT_BY_NAME,
            COMPLETE_PROJECT_BY_INDEX, COMPLETE_PROJECT_BY_ID, COMPLETE_PROJECT_BY_NAME,
            INFO, ABOUT, VERSION, HELP, LIST_COMMANDS, LIST_ARGUMENTS, EXIT
    };

    private static final String[] ARGUMENTS = initArguments();

    private static final String[] TERMINAL_COMMANDS = initTerminalCommands();

    private static String[] initArguments() {
        List<String> arrayList = new ArrayList<>();
        for (Command command : COMMANDS) {
            if (command.getArg() != null && !command.getArg().isEmpty())
                arrayList.add(command.getArg());
        }
        return arrayList.toArray(new String[0]);
    }

    private static String[] initTerminalCommands() {
        List<String> arrayList = new ArrayList<>();
        for (Command command : COMMANDS) {
            if (command.getName() != null && !command.getName().isEmpty())
                arrayList.add(command.getName());
        }
        return arrayList.toArray(new String[0]);
    }

    @Override
    public Command[] getAllCommands() {
        return COMMANDS;
    }

    @Override
    public String[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getArguments() {
        return ARGUMENTS;
    }

}
