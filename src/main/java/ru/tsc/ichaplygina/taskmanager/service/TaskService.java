package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public boolean isEmpty() {
        return taskRepository.isEmpty();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return taskRepository.findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final Task task = new Task(name, description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(final int index) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index);
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final int index) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index);
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (!taskRepository.isFoundById(id)) throw new TaskNotFoundException();
        return taskRepository.update(id, name, description);
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index);
        final String id = taskRepository.getId(index);
        if (!taskRepository.isFoundById(id)) throw new TaskNotFoundException();
        return updateById(id, name, description);
    }

    @Override
    public Task updateStatus(final String id, final Status status) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (!taskRepository.isFoundById(id)) throw new TaskNotFoundException();
        return taskRepository.updateStatus(id, status);
    }

    @Override
    public Task startById(final String id) throws AbstractException {
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task startByIndex(int index) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index);
        final String id = taskRepository.getId(index);
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task startByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = taskRepository.getId(name);
        if (id == null) throw new TaskNotFoundException();
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task completeById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        return updateStatus(id, Status.COMPLETED);
    }

    @Override
    public Task completeByIndex(int index) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index);
        final String id = taskRepository.getId(index);
        return updateStatus(id, Status.COMPLETED);
    }

    @Override
    public Task completeByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = taskRepository.getId(name);
        if (id == null) throw new TaskNotFoundException();
        return updateStatus(id, Status.COMPLETED);
    }

}
