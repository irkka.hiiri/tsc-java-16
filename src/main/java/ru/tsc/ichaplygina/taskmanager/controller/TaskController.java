package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.controller.ITaskController;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showRemoveResult() {
        printLinesWithEmptyLine(TASK_REMOVED);
    }

    private void showUpdateResult() {
        printLinesWithEmptyLine(TASK_UPDATED);
    }

    @Override
    public void clear() {
        final int count = taskService.getSize();
        taskService.clear();
        printLinesWithEmptyLine(count + TASKS_CLEARED);
    }

    @Override
    public void create() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        taskService.add(name, description);
        printLinesWithEmptyLine(TASK_CREATED);
    }

    @Override
    public void showList() {
        if (taskService.isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND);
            return;
        }
        Comparator<Task> comparator = readComparator();
        printListWithIndexes(taskService.findAll(comparator));
    }

    @Override
    public void showTask(final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        printLinesWithEmptyLine(task);
    }

    @Override
    public void showById() throws AbstractException {
        final String id = readLine(ID_INPUT);
        final Task task = taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showByIndex() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        final Task task = taskService.findByIndex(index - 1);
        showTask(task);
    }

    @Override
    public void showByName() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        final Task task = taskService.findByName(name);
        showTask(task);
    }

    @Override
    public void removeById() throws AbstractException {
        final String id = readLine(ID_INPUT);
        if (taskService.removeById(id) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

    @Override
    public void removeByName() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        if (taskService.removeByName(name) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

    @Override
    public void removeByIndex() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (taskService.removeByIndex(index - 1) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

    @Override
    public void startByName() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        if (taskService.startByName(name) == null) throw new TaskNotFoundException();
        showUpdateResult();
    }

    @Override
    public void startById() throws AbstractException {
        final String id = readLine(ID_INPUT);
        taskService.startById(id);
        showUpdateResult();
    }

    @Override
    public void startByIndex() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        taskService.startByIndex(index - 1);
        showUpdateResult();
    }

    @Override
    public void completeByName() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        taskService.completeByName(name);
        showUpdateResult();
    }

    @Override
    public void completeById() throws AbstractException {
        final String id = readLine(ID_INPUT);
        taskService.completeById(id);
        showUpdateResult();
    }

    @Override
    public void completeByIndex() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        taskService.completeByIndex(index - 1);
        showUpdateResult();
    }

    @Override
    public void updateById() throws AbstractException {
        final String id = readLine(ID_INPUT);
        if (this.taskService.findById(id) == null) throw new TaskNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        taskService.updateById(id, name, description);
        showUpdateResult();
    }

    @Override
    public void updateByIndex() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (this.taskService.findByIndex(index - 1) == null) throw new TaskNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        taskService.updateByIndex(index - 1, name, description);
        showUpdateResult();
    }

}
