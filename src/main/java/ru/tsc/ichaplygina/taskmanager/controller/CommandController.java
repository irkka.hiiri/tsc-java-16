package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.controller.ICommandController;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.util.NumberUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int numberOfCpus = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryDisplayed;
        if (maxMemory == Long.MAX_VALUE) maxMemoryDisplayed = SYSINFO_NO_LIMIT_TEXT;
        else maxMemoryDisplayed = NumberUtil.convertBytesToString(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        printLinesWithEmptyLine(SYSINFO_PROCESSORS + numberOfCpus,
                SYSINFO_FREE_MEMORY + NumberUtil.convertBytesToString(freeMemory),
                SYSINFO_MAX_MEMORY + maxMemoryDisplayed,
                SYSINFO_TOTAL_MEMORY + NumberUtil.convertBytesToString(totalMemory),
                SYSINFO_USED_MEMORY + NumberUtil.convertBytesToString(usedMemory));
    }

    @Override
    public void showCommands() {
        printLinesWithEmptyLine((Object[]) commandService.getTerminalCommands());
    }

    @Override
    public void showArguments() {
        printLinesWithEmptyLine((Object[]) commandService.getArguments());
    }

    @Override
    public void showHelp() {
        System.out.println(APP_HELP_HINT_TEXT);
        printLinesWithEmptyLine((Object[]) commandService.getAllCommands());
    }

    @Override
    public void showVersion() {
        printLinesWithEmptyLine(APP_VERSION);
    }

    @Override
    public void showAbout() {
        printLinesWithEmptyLine(APP_ABOUT);
    }

    @Override
    public void showUnknown(final String arg) throws AbstractException {
        throw new CommandNotFoundException(arg);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}