package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.controller.IProjectTaskController;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    private void showRemoveTaskFromProjectResult() {
        printLinesWithEmptyLine(TASK_REMOVED_FROM_PROJECT);
    }

    private void showAddTaskToProjectResult() {
        printLinesWithEmptyLine(TASK_ADDED_TO_PROJECT);
    }

    private void showRemoveProjectResult() {
        printLinesWithEmptyLine(PROJECT_REMOVED);
    }

    @Override
    public void addTaskToProject() throws AbstractException {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        projectTaskService.addTaskToProject(projectId, taskId);
        showAddTaskToProjectResult();
    }

    @Override
    public void clearAllProjects() {
        final int count = projectTaskService.getProjectSize();
        projectTaskService.clearProjects();
        printLinesWithEmptyLine(count + PROJECTS_CLEARED);
    }

    @Override
    public void showTaskListByProjectId() throws AbstractException {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final Comparator<Task> taskComparator = readComparator();
        final List<Task> taskList = projectTaskService.findAllTasksByProjectId(projectId, taskComparator);
        if (taskList == null) throw new ProjectNotFoundException();
        if (taskList.isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND_IN_PROJECT);
            return;
        }
        printListWithIndexes(taskList);
    }

    @Override
    public void removeTaskFromProject() throws AbstractException {
        final String projectId = readLine(PROJECT_ID_INPUT);
        final String taskId = readLine(TASK_ID_INPUT);
        projectTaskService.removeTaskFromProject(projectId, taskId);
        showRemoveTaskFromProjectResult();
    }

    @Override
    public void removeProjectById() throws AbstractException {
        final String id = readLine(ID_INPUT);
        if (projectTaskService.removeProjectById(id) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

    @Override
    public void removeProjectByName() throws AbstractException {
        final String name = readLine(NAME_INPUT);
        if (projectTaskService.removeProjectByName(name) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

    @Override
    public void removeProjectByIndex() throws AbstractException {
        final int index = readNumber(INDEX_INPUT);
        if (projectTaskService.removeProjectByIndex(index - 1) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

}
