package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    private static final String PROJECT_NOT_FOUND = "Project not found.";

    public ProjectNotFoundException() {
        super(PROJECT_NOT_FOUND);
    }

}
