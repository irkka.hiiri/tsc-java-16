package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    private static final String TASK_NOT_FOUND = "Task not found.";

    public TaskNotFoundException() {
        super(TASK_NOT_FOUND);
    }

}
