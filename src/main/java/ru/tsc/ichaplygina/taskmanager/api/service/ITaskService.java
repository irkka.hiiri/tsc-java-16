package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task add(final String name, final String description) throws AbstractException;

    void clear();

    Task completeById(final String id) throws AbstractException;

    Task completeByIndex(final int index) throws AbstractException;

    Task completeByName(final String name) throws AbstractException;

    List<Task> findAll(final Comparator<Task> comparator);

    Task findById(final String id) throws AbstractException;

    Task findByName(final String name) throws AbstractException;

    Task findByIndex(final int index) throws AbstractException;

    int getSize();

    boolean isEmpty();

    Task removeById(final String id) throws AbstractException;

    Task removeByIndex(final int index) throws AbstractException;

    Task removeByName(final String name) throws AbstractException;

    Task startById(final String id) throws AbstractException;

    Task startByIndex(final int index) throws AbstractException;

    Task startByName(final String name) throws AbstractException;

    Task updateByIndex(final int index, final String name, final String description) throws AbstractException;

    Task updateById(final String id, final String name, final String description) throws AbstractException;

    Task updateStatus(String id, Status status) throws AbstractException;

}
