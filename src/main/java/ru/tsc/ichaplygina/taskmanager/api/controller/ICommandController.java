package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;

public interface ICommandController {

    void showAbout();

    void showArguments();

    void showCommands();

    void showHelp();

    void showSystemInfo();

    void showVersion();

    void showWelcome();

    void showUnknown(String arg) throws AbstractException;

    void exit();

}
