package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

public interface IProjectTaskController {

    void addTaskToProject() throws AbstractException;

    void clearAllProjects();

    void removeProjectById() throws AbstractException;

    void removeProjectByIndex() throws AbstractException;

    void removeProjectByName() throws AbstractException;

    void removeTaskFromProject() throws AbstractException;

    void showTaskListByProjectId() throws AbstractException;

}
