package ru.tsc.ichaplygina.taskmanager.api.controller;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

public interface ITaskController {

    void clear();

    void completeById() throws AbstractException;

    void completeByIndex() throws AbstractException;

    void completeByName() throws AbstractException;

    void create() throws AbstractException;

    void removeById() throws AbstractException;

    void removeByIndex() throws AbstractException;

    void removeByName() throws AbstractException;

    void showTask(Task task) throws AbstractException;

    void showById() throws AbstractException;

    void showByIndex() throws AbstractException;

    void showByName() throws AbstractException;

    void showList();

    void startById() throws AbstractException;

    void startByIndex() throws AbstractException;

    void startByName() throws AbstractException;

    void updateById() throws AbstractException;

    void updateByIndex() throws AbstractException;

}
